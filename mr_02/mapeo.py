#mapeo.py
import sys
#cat MATRICULADOS_POR_PROGRAMA.csv | python3 codigo_MR/mr_02/codigo_MR/mr_02/mapeo.py
# entrada viene de STDIN (standard input)

try:
    for line in sys.stdin:
        line = line.strip()
        line = line.split(",")

        #Emitir llave valor de cada facultad para la vigencia 2019-1
        if line[0] == '2019-1':
            #El campo 11 es FACULTAD
            print (f'{line[11]} \t 1' )
except:
    pass

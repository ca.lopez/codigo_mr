#reduccion.py
'''
cat MATRICULADOS_POR_PROGRAMA.csv | python3 codigo_MR/mr_02/mapeo.py |
python3 codigo_MR/mr_02/reduccion.py
'''

import sys

salida = {}

#Intercambio
for line in sys.stdin:
    line = line.strip()
    vigencia, valor = line.split('\t')
    #print edad, diag
    if vigencia in salida:
        salida[vigencia].append(int(valor))
    else:
        salida[vigencia] = []
        salida[vigencia].append(int(valor))

#Reduccion
for vigencia in salida.keys():
    sum_v = len(salida[vigencia])
    print(vigencia, sum_v)

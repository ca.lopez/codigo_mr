#mapeo.py
import sys
#cat MATRICULADOS_POR_PROGRAMA.csv | python3 codigo_MR/mr_01/codigo_MR/mr_01/mapeo.py
# entrada viene de STDIN (standard input)

try:
    for line in sys.stdin:
        line = line.strip()
        line = line.split(",")

        #caso 1: Emitir todos los registros
        #print (line)

        #caso 2: emitir llave valor de vigencia
        # de la forma 2019-1  1
        print (f'{line[0]} \t 1' )
except:
    pass
